var { MongoClient } = require('mongodb');
var url = 'mongodb://localhost:27017/mydb';
 
MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
  if (err) throw err;
  console.log('Database created!');
  var dbo = db.db();
  dbo.createCollection('customers', (err, res) => {
    if (err) throw err;
    console.log('Collection created!');
    var myobj = { id: 2, name: "Company Inc", address: "Highway 37" };
    dbo.collection("customers").insertOne(myobj, function(err, res) {
      if (err) throw err;
      console.log("1 document inserted");
      db.close();
    });
  });
});