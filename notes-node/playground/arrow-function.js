// Function in multiple lines
var square = (x) => {
    var result = x * x;
    return result;
};
console.log(square(9));
// Function in one line
var square2 = x => x * x;
console.log(square2(9));

var user = {
    name: 'Melvin',
    sayHi: () => {
        console.log(`Hi. I'm ${this.name}`);
    },
    sayHiAlt () {
        console.log(arguments);
        console.log(`Hi. I'm ${this.name}`);
    }
}

user.sayHi(1,2,3);

// WITH ARROW FUNCTION IT IS NOT POSSIBLE TO USE this ACCESOR

// USE ARROW FUNCTION IF ALWAYS, CHANGE IT IF YOU NEED TO USE THIS OR ARGUMENTS PARAMETER

