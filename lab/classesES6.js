class Animal {
    makeSound() {
        return this._sound;
    }
}

class Dog extends Animal {
    constructor(name) {
        super();
        this._name = name;
        super._sound = "Barfff!";
    }

    get name() { 
        console.log("Estoy ejecutando el getter");
        return this._name;
    }

    set name(value) {
        console.log("Estoy ejecutando el setter"); 
        this._name = value; 
    }
}

var myDog = new Dog("Wifi");
console.log(myDog);
console.log(myDog._name);
console.log(myDog.name);
console.log(myDog.name = "Colon" );