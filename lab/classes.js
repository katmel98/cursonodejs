function Person() {
    this.id = 0;
    this.name = null;
    this.email = null;

    this.whoami = () => {
        console.log(this.id , this.name, this.email);
        return 'done';
    }
}

var person = new Person();

person.id = 1;
person.name = 'Maduro';
person.email = 'has_gone@done.come';

console.log(person.whoami());
