function Person() {
    this.id = 0;
    this.name = null;
    this.email = null;
    this.whoami = function () {
        console.log(this.id, this.name, this.email)
        return 'done';
    }
}

Person.prototype.age = 20;
var that = Person.prototype;
Person.prototype.isOld = () => {
    console.log("Estoy evaluando isOld");
    console.log(that.age);
    return that.age >= 18;
};

var person = new Person();
console.log(person.isOld());

console.log(person);
console.log(Person.prototype);