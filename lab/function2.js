function sumTwoParams (one, two) {
    one = (one) ? one: 0;
    two = (two) ? two: 0;
    return one + two;
}

var sumThreeParams = (one, two, three) => {
    one = (one) ? one: 0;
    two = (two) ? two: 40;
    three = (three) ? three: 0;
    return one + two + three;
}

console.log(sumTwoParams(2,2));
console.log(sumThreeParams(3,6,5));