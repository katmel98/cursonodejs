function getMaxValue(array) {
    var maxValue = 0;

    for (var index = 0; index  < array.length; index++) {
        var element = array[index];
        if(element > maxValue) {
           maxValue = element;
        }
    }
    return maxValue;
}

var myArray = [1,25,1,94,12,5,4,5,6,124,400,123];
var maxValue = getMaxValue(myArray);
console.log("maxValue == ", maxValue);