// EJEMPLO DE INSTRUCCIONES DE CONTROL
for(let var1 = 1; var1 <= 10; var1++){
    if(var1 == 7) continue;
    console.log(var1);
}

let collection = [];
let obj = {
    id: 1,
    name: "Willy Wonka",
    email: "ww@chocolateFactory.com"
}
collection.push(obj);
obj = {
    id: 2,
    name: "Willy Wonka 2",
    email: "ww2@chocolateFactory.com"
}
collection.push(obj);

for (let element in collection) {
    if (element == 0) break;
    console.log(collection[element]);
}



// // EJEMPLOS FOR .. IN
// let obj = {
//     id: 1,
//     name: "Willy Wonka",
//     email: "ww@chocolateFactory.com"
// };

// for (let attrName in obj) {
//     console.log(attrName + ": " + obj[attrName]);
// }


// EJEMPLOS WHILE Y DO .. WHILE 

// let var1 = 1;
// console.log("Bucle 'while' ...");
// while (var1 <= 10) {
//     console.log(var1);
//     var1++;
// }
// console.log("Bucle 'do .. while' ...");
// let var2 = 0;
// do {
//     var2++;
//     console.log(var2);
// } while (var2 <= 10);

// EJEMPLO COMPARACION SWITCH

// var var2 = "One";

// switch (var2) {
//     case "One":
//         console.log("Es 'One': ", var2);
//         break;
//     case "Two":
//         console.log("Es 'Two': ", var2);
//         break;
//     case "Three":
//         console.log("Es 'Three': ", var2);
//         break;
//     default: 
//         console.log("Otro valor: ", var2);
//         break;
// }


// Ejemplo if ... else ... ifelse

// var var1 = 1;

// if (var1 > 0) {
//     console.log("Mayor a cero: ", var1);
// } else {
//     console.log("Menor a cero: ", var1);
// }

// var var2 = "Three";
// if (var2 == "One") {
//     console.log("Es 'One': ", var2 );
// } else if (var2 == "Two") {
//     console.log("Es 'Two': ", var2);
// } else {
//     console.log("El valor es ", var2)
// }

// var a = 2;
// var b = "2";

// if (a === b) {
//     console.log("Si es igual")
// } else {
//     console.log("No es igual");
// }



// Ejemplo de problema de ámbito

// var temporal = "temporal";
// for(var i = 1; i != false; i = false) {
//     let temporal = 10;
// }
// console.log(i, temporal);
// var x = 1
// var u = 35;
// console.log(u + x)
// u = "Esta es una prueba";
// console.log(u + x);

// Ejemplo de constantes

// const const1 = 20
// console.log(const1)

// const1 = 1;
// console.log(const1);

// const const2;
// const2 = 50;


// Ejemplo de ámbito de variables

// var1 = "Cadena de caracteres";
// console.log(var1);

// {
//     let var2 = 123;
//     console.log(var2, var1)
// }

// console.log(var1 + " " + var2);