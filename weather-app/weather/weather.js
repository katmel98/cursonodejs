const request = require('request');

var getWeather = (lat, long, callback) => {
    request ({
        url: `https://api.forecast.io/forecast/98634ad54a0320c7e2141068b8235dab/${lat},${long}`,
        json:true
    },(error, response, body) => {
        if(error) {
            callback("Unable to connect to Forecast.io servers");
        } else if (response.statusCode === 400) {
            callback("Unable to fetch weather");
        } else if (response.statusCode === 200) {
            callback(undefined, {
                temperature: body.currently.temperature,
                apparentTemperature: body.currently.apparentTemperature
            });
        }
    })    
}

module.exports.getWeather = getWeather;





