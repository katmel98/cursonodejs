const yargs = require('yargs');
const axios = require('axios');

const argv = yargs
    .options({
        a: {
            demand: true,
            alias: 'address',
            describe: 'Address to fetch weather for',
            string: true    
        }
    })
    .help()
    .alias('help', 'h')
    .argv; 

var encodedAddress = encodeURIComponent(argv.address); 
var geocodeUrl = `https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}&key=AIzaSyB2sEk2xtVZHAaGmvcVLViOIaFF__pgW9M`;

axios.get(geocodeUrl)
.then((response) => {
    if(response.data.status === 'ZERO_RESULTS'){
        throw new Error('Unable to find that address');
    } 

    var lat = response.data.results[0].geometry.location.lat;
    var long = response.data.results[0].geometry.location.lng;
    var weatherUrl = `https://api.forecast.io/forecast/98634ad54a0320c7e2141068b8235dab/${lat},${long}`;

    console.log(response.data.results[0].formatted_address);

    return axios.get(weatherUrl);

}).then((response) => {
    var temperature = response.data.currently.temperature;
    var apparentTemperature = response.data.currently.apparentTemperature;
    console.log(`It's currently ${temperature}. It feels like ${apparentTemperature}`);
})
.catch((error) => {
    if(error.code === 'ENOTFOUND') {
        console.log('Unable to connect API servers.');
    } else {
        console.log(error.message);
    }
});