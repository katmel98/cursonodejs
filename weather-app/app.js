const yargs = require('yargs');

const geocode = require('./geocode/geocode');
const weather = require('./weather/weather');

const argv = yargs
    .options({
        a: {
            demand: true,
            alias: 'address',
            describe: 'Address to fetch weather for',
            string: true    
        }
    })
    .help()
    .alias('help', 'h')
    .argv; 

geocode.geocodeAddress(argv.address, (errorMessages, results) => {
    if(errorMessages) {
        console.log(errorMessages);
    } else {
        console.log(results.address);
        weather.getWeather(results.latitude, results.longitud, (errorMessages, weatherResults) => {
            if (errorMessages) {
                console.log(errorMessages);
            } else {
                console.log(`It's currently ${weatherResults.temperature}. It feels like ${weatherResults.apparentTemperature}.`);
            }
        });        
    }
});

