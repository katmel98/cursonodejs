// ES5 Event Emitter Example

var events = require('events').EventEmitter;
var emitter = new events.EventEmitter();

emitter.on('newEvent', function(user){
    console.log(user);
});

emitter.emit('newEvent', "Krunal");


// Ejemplo heredando las características del EventEmmiter desde una clase

// var EventEmitter = require('events').EventEmitter;
// var util = require('util');

// var User = function(username){
//     this.username = username;
// }
// util.inherits(User, EventEmitter);
// var user = new User('Krunal Lathiya');
// user.on('nuevent', function(props){
//     console.log(props);
// });
// user.emit('nuevent', 'dancing');



// Ejemplo ES6

// const EventEmitter = require('events');

// class MyEmitter extends EventEmitter {}

// const myEmitter = new MyEmitter();
// myEmitter.on('event', () => {
//   console.log('an event occurred!');
// });
// myEmitter.emit('event');