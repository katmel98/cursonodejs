const { MongoClient } = require('mongodb');
const url = 'mongodb://localhost:27017/mydb';

MongoClient.connect(url, 
                    { useNewUrlParser: true }, 
                    function(err, database) {
    if (err) throw err;
    // console.log('Database created!');
    var dbo = database.db();
    // Crear coleccion dentro de la bbdd
    // dbo.createCollection('customers', (err, res) => {
    //     if (err) throw err;
    //     console.log('Collection created!');
    //     database.close();
    // });

    // ejemplo de insercion de objetos: insertOne, insertMany
    // var objeto = [
    //         { _id: "58fdbf5c0ef8a50b4cdd9a84" , name: 'John', address: 'Highway 71'},
    //         { _id: "58fdbf5c0ef8a50b4cdd9a85" , name: 'Peter', address: 'Lowstreet 4'},
    //         { _id: "58fdbf5c0ef8a50b4cdd9a86" , name: 'Amy', address: 'Apple st 652'},
    //         { _id: "58fdbf5c0ef8a50b4cdd9a87" , name: 'Hannah', address: 'Mountain 21'},
    //         { _id: "58fdbf5c0ef8a50b4cdd9a88" , name: 'Michael', address: 'Valley 345'},
    //         { _id: "58fdbf5c0ef8a50b4cdd9a89" , name: 'Sandy', address: 'Ocean blvd 2'},
    //         { _id: "58fdbf5c0ef8a50b4cdd9a8a" , name: 'Betty', address: 'Green Grass 1'},
    //         { _id: "58fdbf5c0ef8a50b4cdd9a8b" , name: 'Richard', address: 'Sky st 331'},
    //         { _id: "58fdbf5c0ef8a50b4cdd9a8c" , name: 'Susan', address: 'One way 98'},
    //         { _id: "58fdbf5c0ef8a50b4cdd9a8d" , name: 'Vicky', address: 'Yellow Garden 2'},
    //         { _id: "58fdbf5c0ef8a50b4cdd9a8e" , name: 'Ben', address: 'Park Lane 38'},
    //         { _id: "58fdbf5c0ef8a50b4cdd9a8f" , name: 'William', address: 'Central st 954'},
    //         { _id: "58fdbf5c0ef8a50b4cdd9a90" , name: 'Chuck', address: 'Main Road 989'},
    //         { _id: "58fdbf5c0ef8a50b4cdd9a91" , name: 'Viola', address: 'Sideway 1633'},
    //     ];
    // dbo.collection('customers').insertMany(objeto, (err, res) => {
    //     if (err) throw err;
    //     console.log(`Se han insertado ${res.insertedCount} documento(s)`);
    //     database.close();    
    // });
    
    // ejemplos de consulta: findOne, find, projection, sort, filter
    // var thesort = { direccion: 1 };
    // var query = {direccion: /.*s/};
    // dbo.collection('customers')
    //     .find().limit(4).toArray((err, result) => {
    //     if (err) throw err;
    //     console.log(result);
    //     database.close();
    // });
    
    // Ejemplo de eliminacion deleteOne, deleteMany
    // var query = { direccion: "Sol"};
    // dbo.collection('customers').deleteOne(query, (err, obj) => {
    //     if(err) throw err;
    //     console.log("Hemos eliminado 1 documento");
    //     console.log(obj);
    //     database.close();

    // })

    // ejemplos eliminacion de colecciones
    // dbo.dropCollection('customers', (err, delOK) => {  

    // dbo.collection('customers').drop((err, delOK) => {  
    //     if(err) throw err;

    //     console.log(delOK);

    //     if (delOK) console.log("Collection deleted!");
        
    //     database.close()
    // })

    // ejemplo de eliminacion

    // var myquery = { direccion: "Sol"};
    // var newvalues = { $set: { name: "Yoda"  }};
    // dbo.collection("customers").updateOne(myquery, newvalues, (err, res) => {
    //     if (err) throw err;
    //     console.log(`SE HA ACTUALIZADO ${res.modifiedCount} DOCUMENTO(S)`);
    //     // console.log(res);
    //     database.close();
    // })

    dbo.collection('orders').aggregate([
        {
            $lookup: {
                from: 'products',
                localField: 'product_id',
                foreignField: '_id',
                as: 'orderdetails'
            }
        }
    ]).toArray((err, result) => {
        if (err) throw err;
        console.log(JSON.stringify(result));
        database.close();
    })

});
