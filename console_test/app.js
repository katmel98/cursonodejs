const minimist = require('minimist');
const { version } = require('./package.json');

const menus = {
    main: `
        node app.js [command] <options>

        today ............... muestra el clime ade hoy
        version ............. muestra la version del paquete
        help ................ muestra el menu de ayuda para un comando`,
    today: `
        node app.js today <options>

        ..location, -l ....... la ubicación a emplear
    `
};
    const args = minimist(process.argv.slice(2));
    
    let cmd = args._[0] || 'help';
    if (args.version || args.v) {
        cmd = 'version';
    }
    if (args.help || args.h) {
        cmd = 'help';
    }

    switch (cmd) {
        case 'today':
            console.log('Hoy el clima está excelente!');
            break;
        case 'version':
            console.log(`v${version}`);
            break;
        case 'help':
            const subcmd = args._[0] === 'help' ? args._[1] : args._[0];            
            console.log(menus[subcmd] || menus.main);
            break;
        default:
            console.error(`"${cmd}" no es un comando válido!`);
            break;


}